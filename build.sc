import mill._, scalalib._

object server extends ScalaModule {
  def scalaVersion = "3.1.2"

  val http4sVersion = "0.23.0"
  val scalatags = "0.11.1"
  val tapir = "1.0.0-M7"
  val pureconfig = "0.17.1"
  val catsParse = "0.3.7"
  val circe = "0.14.1"


  def ivyDeps = Agg(
    ivy"org.http4s::http4s-blaze-server:$http4sVersion",
    ivy"com.lihaoyi::scalatags:$scalatags",
    ivy"com.softwaremill.sttp.tapir::tapir-http4s-server:$tapir",
    ivy"com.github.pureconfig::pureconfig-core:$pureconfig",
    ivy"org.typelevel::cats-parse:$catsParse", 
    ivy"io.circe::circe-core:$circe",
    ivy"io.circe::circe-parser:$circe"
  )

  def scalacOptions = Seq("-Ysafe-init")


  object test extends TestModule.Munit with Tests {
    def scalacOptions = Seq("-Ysafe-init")
    def ivyDeps = Agg(
      ivy"org.scalameta::munit:1.0.0-M3"
    )
  }
}
