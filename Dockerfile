FROM docker.io/ibm-semeru-runtimes:open-17.0.2_8-jdk

WORKDIR /dist
ADD build.sc .
ADD server/ server/
ADD millw .
ADD .mill-version .

RUN ./millw server.assembly

EXPOSE 8080

ENTRYPOINT java -Dport=8080 -jar out/server/assembly.dest/out.jar