Computer programs are relatively predictable. If you write `4 + 8` in the Scala console, you will always get `12` unless something is horribly wrong with your computer. In order for a program to be relatively unpredictable, we need to provide it one of two things: user input, or randomness. In this unit, we'll learn how to introduce randomness to our programs.

If you have any issues understanding the material in this unit, please ask questions in the [discord channel](https://discord.gg/ytNbazb9) I've set up for teaching Scala, or you can discuss on [scala-users](https://users.scala-lang.org/).

Please remember to run the provided examples via the Scala console unless told otherwise. If you've forgotten how to launch and/or shut down the Scala console, please refer [here](https://mhammons.hashnode.dev/data-values-and-operators#heading-scala-console).

# Random

In programs, it's sometimes helpful to be given random information. For example, if you're making a game, it'd be very helpful to have the computer behave somewhat unpredictably if you wanted to have a challenging game. It is for this reason that the Scala standard library provides access to `Random`, and a number of helpful methods that generate random data for you. `Random` is not available to your program by default though.

```scala
scala> Random.nextInt()
-- [E006] Not Found Error: -----------------------------------------------------
1 |Random.nextInt()
  |^^^^^^
  |Not found: Random
```

In order to make it available, we either have to use its full name, or use an `import` statement.

```scala
scala> scala.util.Random.nextInt()
val res0: Int = -123228248

scala> import scala.util.Random

scala> Random.nextInt()
val res1: Int = -1048161472
```

When using `Random` and its associated methods, you should import it into your program with the `import` statement shown above.

`Random` is host to a number of very helpful methods that can help you generate and use random data.

## Random.nextInt

`Random.nextInt` is used to generate a random whole number from the range of `-2147483648` to `2147483647`.