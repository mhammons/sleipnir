In a previous unit, we discussed if expressions, and saw how they can allow our program to adapt to different inputs, and change its behavior. If expressions can do a lot of things, but they are typically rather verbose. For example, if we want to know if a value `a` has the text `"hello"` inside it, we have to write out the boolean expression `a == "hello"`. If we want to know if it has either `"hello"` or `"goodbye"` in it, we need to write `a == "hello" || a == "goodbye"`. As the complexity of what we want to check grows, the length and verbosity of the boolean expressions we have to write grows as well.

In Scala, we have a better way to check things like this. It's called pattern matching, and it is a central feature of the Scala language.

If you have any issues understanding the material in this unit, please ask questions in [the discord channel](https://discord.gg/ytNbazb9) I've set up for teaching Scala, or you can discuss on scala-users.

Please remember to run the provided examples via the Scala console unless told otherwise. If you've forgotten how to launch and/or shut down the Scala console, please refer [here](https://mhammons.hashnode.dev/data-values-and-operators#heading-how-to-run-scala-code).

# Match expressions

Lets think about a method definition. We want to check if a number is a prime number less than 10, and we want to do this 


