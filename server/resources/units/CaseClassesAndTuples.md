If I asked you to create a list of employees at a company, how would you do that in Scala? With what we've learned so far, we might write a list of employee names:

```
scala> List("Adam Johnson", "Kanye West")
val res0: List[String] = List(Adam Johnson, Kanye West)
```

This works if all we want is to store their names, but what if we needed to store their ages, email, and their pay in this `List`? Things quickly become difficult for us:

```
scala> val employees = List("Adam Johnson", 34, "adam.johnson@gmail.com", 4000, "Kanye West", 52, "yeezy@yahoo.com", 4535)
val employees: List[Matchable] = List(Adam Johnson, 34, adam.johnson@gmail.com, 4000, Kanye West, 52, yeezy@yahoo.com, 4535)

scala> val strEmployees = List("Adam Johnson;34;adam.johnson@gmail.com;4000", "Kanye West;52;yeezy@yahoo.com;4535")
val strEmployees: List[String] = List(Adam Johnson;34;adam.johnson@gmail.com;4000, Kanye West;52;yeezy@yahoo.com;4535)
```

Neither of these solutions are good. The first has lost the information about the data in the `List`, so we just have `Matchable` now. Likewise, if I asked you to calculate the average age of the employees, you'd have to rely to some fairly hacky and not assuredly correct code to get a result. Your first issue would arise from the fact that the `size` method of `employees` would give you the number of pieces of data on the employees, not the number of employees. The second is you would have to extract only the ages from the list, get them back into `Int` form, and sum them together.

The second try maintains type information, but now we need to parse `String`s to get the information that's been written back out in a usable state.

This problem of needing to group data together is made simple with a concept that we haven't covered yet: case classes.

`List`s provide a great way to group similar types of data together in order.

If you have any issues understanding the material in this unit, please ask questions in [the discord channel](https://discord.gg/ytNbazb9) I've set up for teaching Scala, or you can discuss on scala-users.

Please remember to run the provided examples via the Scala console unless told otherwise. If you've forgotten how to launch and/or shut down the Scala console, please refer [here](https://mhammons.hashnode.dev/data-values-and-operators#heading-how-to-run-scala-code).

# Case Classes

It is not uncommon that we need to group multiple small pieces of information into a larger, singular package. It is even more common that we need quick and easy access to the original, small pieces of information. Imagine a tool chest; it lets you carry a large set of tools with you with ease, but it's also designed to let you pull out the tools one by one and use them again. 

It is for this purpose that case classes exist in Scala; they allow you to group together numbers, text, lists, and other data in a bigger whole. That bigger whole lets you easily transport that information around, while still giving you access to the small parts you wanted before. Let's look at the syntax to declare a case class in Scala:

```
case class <name>(<name>: <type>, <name>: <type>)
```

So far, it looks a bit like a method definition, but it has no body and uses the keywords `case` and `class`. The first name you provide is the name the case class will have. We'll call this name the "class name", and the normal naming rules apply except that we typically capitalize the first letter of the name. That's because this class name will become a new type in our program! You may have noticed, but the names of all the types in Scala start with a capital letter: `Int`, `String`, `List`, `Unit`, `Any`, etc. You should follow this convention so that when people see your class' name, they know they're looking at a type. Inside the parentheses, a series of parameters can be defined; one or more names with type information associated with them. These class parameters will be the data that comprises your class. 

Now that we've seen how to define a case class, lets see how it applies to the problem we had up above. We wanted to store information about employees, so we should probably name our case class `Employee`...

```scala
scala> case class Employee(name: String, age: Int, email: String, income: Int)
// defined case class Employee
```
The output of the Scala console should tell you about whether this case class syntax is a statement or expression. As stated in earlier units, definitions in Scala are almost always statements, and this one is no different.

So now we have our case class, but how do we use it? Well, it's quite easy:

```scala
scala> val yeezy = Employee("Kanye West", 53, "yeezy@yahoo.com", 5242)
val yeezy: Employee = Employee(Kanye West, 53, yeezy@yahoo.com, 5242)
```

Creating this grouped together data takes the form of using `()` parentheses with the class name, and the data you wish to store. This is called construction in Scala. When you construct a case class, you get an instance of that case class back. In order to construct a case class, you must provide data for all the parameters you defined the class with, or Scala will give you an error:

```scala
scala> val yeezy = Employee("Kanye West", 53, "yeezy@yahoo.com")
-- Error: ----------------------------------------------------------------------
1 |val yeezy = Employee("Kanye West", 53, "yeezy@yahoo.com")
  |            ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
  |missing argument for parameter income of method apply in object Employee: (name: String, age: Int, email: String, income: Int): Employee
```

Please note that just like with methods, the order of the input data is important for case class construction. Putting data in in the wrong order may result in confusingly labeled data (like putting the email where the name should be), or a type error (like putting the age where the email should be). If you want to provide the construction data in a different order, use the parameter names just like you can do with methods...

```scala
scala> val yeezy = Employee(age = 53, name = "Kanye West", income = 40000, email = "yeezy@yahoo.com")
val yeezy: Employee = Employee(Kanye West, 53, yeezy@yahoo.com, 40000)
```

> Case class definitions create new types for you to use. These types can be used as inputs to methods, functions, and stored in values like everything else.


So now we know how to define an `Employee` case class, and construct it with some data. But how can we use it? For example, we were promised information would be as easy to take out as it would be to put it in. In order to extract information from our `Employee`, we only need to use the `.` operator and a parameter name:

```scala
scala> yeezy.name
val res0: String = Kanye West

scala> yeezy.age
val res1: 53
```

Now that we know how to extract information from our case class, lets try to tackle a problem we had before: we have a `List[Employee]`, and we want to know the average employee age. How do we calculate that? It's exceedingly simple with the polymorphic list methods we learned last unit, and the case classes we learned this one!

```scala
scala> val employees = 
   List(
      Employee("Adam Johnson", 34, "adam.johnson@gmail.com", 4500),
      Employee("Kanye West", 43, "yeezy@yahoo.com", 6300)
   )

val employees: List[Employee] = List(Employee(Adam Johnson,34,adam.johnson@gmail.com,4500), Employee(Kanye West,43,yeezy@yahoo.com,6300))

scala> employees.map(employee => employee.age).sum / employees.size.toDouble
val res0: Double = 38.5
```

Thanks to the data-grouping case classes provide the size of our employees list corresponds to the number of employees. With that, we just need to perform four simple steps:

1. transform our list of employees to a list of ages using the `map` method
2. add together all the ages in the new list using the `sum` method
3. get the number of employees with the `size` method and make it fractional with the `toDouble` method (so that we can get a fractional average age)
4. divide the total age by the number of employees

Lets look at the steps one by one in the console:

```scala
scala> val employeeAges = employees.map(employee => employee.age) //step 1
val employeeAges: List[Int] = List(34, 43)

scala> val totalAge = employeeAges.sum //step 2
val totalAge: Int = 77

scala> val numberOfEmployees = employees.size.toDouble //step 3
val numberOfEmployees: Double = 2.0

scala> totalAge / numberOfEmployees //step 4
val res0: Double = 38.5

scala> //and now all the steps on one line

scala> employees.map(employee => employee.age).sum / employees.size.toDouble
val res1: Double = 38.5
``` 

We can follow similar steps to get the average wage of the employees. It's also fairly easy to sort the employees by email address, name, age, or wage with the `sortBy` method we learned last unit.

Case classes can even store other case classes. A good example would be the `name` parameter of our current `Employee` case class; sure we can get the whole name easily, but what about if we want to only have the first name? Or what about if we want the last name?

```scala
scala> case class Name(firstName: String, lastName: String)
// defined case class Name

scala> case class Employee(name: Name, age: Int, email: String, income: Int)
// defined case class Employee

scala> val yeezy = Employee(Name("Kanye", "West"), 53, "yeezy@yahoo.com", 6320)
val yeezy: Employee = Employee(Name(Kanye,West),53,yeezy@yahoo.com,6320)

scala> yeezy.name.firstName
val res0: String = "Kanye"
```

Case classes can group types together, and this includes complex types like case classes, so having `Employee` use `Name` to store name data is quite simple.

With this basic functionality, case classes has greatly improved our ability to write programs. However, case classes have more to them than just grouping data together and allowing easy access to data...

## Equality

Case classes are equal to each other based on the data they contain. Take for example the name "Adam Johnson"...

```scala
scala> val nameA = Name("Adam", "Johnson")
val nameA: Name = Name(Adam, Johnson)

scala> val nameB = Name("Adam", "Johnson")
val nameB: Name = Name(Adam, Johnson)

scala> nameA == nameB
val res0: Boolean = true
```

## Changing a case class' data

Let's say you wanted to give all of the employees in our records a $500 raise. In order to do this, you might want to just recreate the employee records with their income increased like this:

```scala
scala> val employees =    
   List(
      Employee(Name("Adam", "Johnson"), 34, "adam.johnson@gmail.com", 4500),
      Employee(Name("Kanye", "West"), 43, "yeezy@yahoo.com", 6300)
   )

scala> employees
   .map( employee => 
      Employee(
         employee.name,
         employee.age,
         employee.email,
         employee.income + 500
      )
   )

val res0: List[Employee] = List(Employee(Name(Adam,Johnson),34,adam.johnson@gmail.com,5000), Employee(Name(Kanye,West),43,yeezy@yahoo.com,6800))
```

This works, but its very verbose. Instead, we can use a method that exists for all case classes called `copy`:

```scala
scala> employees
   .map( employee => 
      employee.copy(income = employee.income + 500)
   )

val res0: List[Employee] = List(Employee(Name(Adam,Johnson),34,adam.johnson@gmail.com,5000), Employee(Name(Kanye,West),43,yeezy@yahoo.com,6800))

```

The `copy` method takes the same parameters as the construction of the case class, but the parameters are all optional. If you don't provide data for them, they are filled in with the original data automatically. You can name a parameter to change, and the way you want it to change, and you will get a new copy of the case class with that bit of data changed. 

Let's say that we wanted to record that all the employees are one year older, and their incomes have doubled. This is how that would look with `copy`.

```scala
scala> employees
   .map(employee => 
      employee.copy(
         age = employee.age + 1,
         income = employee.income * 2
      )
   )

val res0: List[Employee] = List(Employee(Name(Adam,Johnson),35,adam.johnson@gmail.com,9000), Employee(Name(Kanye,West),44,yeezy@yahoo.com,12600))
```

## Practice

1. Define a case class that stores information about a house. It should have parameters for the street address, size of the land it's on, the size of the house itself, and its cost.
2. Construct an `Employee` named "John Doe" who is 32 years old, has the email "john.doe@aol.com" and has an income of 5 dollars per month.
3. Is a case class definition a statement or an expression? Can you define a case class inside a code block? Can you define one inside a method? (try if you're not sure)
4. Define a method that takes an `Employee` and outputs young if they're 20 or younger, middle aged if they're 21-50, and old if they're older than 50.
5. **HARD** Define a case class inside the definition of a value, and make the value be an instance of that case class. What is the result of this? Why do you think the result is what it is?

# Tuples

In a previous unit we learned about functions. They let us have reuseable pieces of code like methods, but we didn't have to have a definition statement for them, and they could be passed into methods and even out of methods:

```scala
scala> def adder(a: Int): Int => Int = (b: Int) => a + b
def adder(a: Int): Int => Int

scala> adder(4)
val res0: Int => Int = ...

scala> res0(5)
val res1: Int = 9
```

Functions are super helpful for creating a quick, reusable block of code without having to define a method. A tuple is the same thing but for a case class: a quick grouping of data that can be used without having to write or need a definition statement.

The syntax for forming a tuple is very simple:

```
(<expression>, <expression>)
```
A tuple can be formed by putting two or more expressions between parentheses. You need only separate each expression by a comma `,`. When you do this, you create a tuple, a temporary, nameless grouping of data that can be passed around. Let's look at how to construct some instances of tuples, and see what their types look like:

```scala
scala> (1,true)
val res0: (Int, Boolean) = (1,true)

scala> ("hello", "world", 52, 820.0)
val res1: (String, String, Int, Double) = (hello, world, 52, 820.0)
```

As you can see, the type of a tuple is fairly simple. It's `()` surrounding a list of the types that form it, with each element in the list being separated from each other using a `,`.

Tuples are helpful when you want to create a grouping of data rapidly, but don't want to give a formal name or description to the grouping. Getting data out of them is easy as well, though not as easy as with a case class...

```scala
scala> val tup = (1, true)
val tup: (Int, Boolean) = (1, true)

scala> tup._1
val res0: Int = 1

scala> tup._2
val res1: Boolean = true
```

Since tuples are like nameless case classes, their parameters don't have names aside from the number indicating their position. The first parameter in a tuple is accessed with `_1`, the second with `_2`, and so on.

So, what's a good example of the usage of tuples? Well, what if we had a `List[String]` that had the favorite meals of each employee in our company called "favorites". We don't have favorite food as part of the `Employee` definition, and we don't want to add it because we just need that information temporarily. We want to take the list of favorite foods and list of employees, and quickly group each employee with their favorite foods so that we can print out what each employee likes.

We can group that data together with each employee in our employee list instantly thanks to tuples and a helpful `List` method named `zip`:

```scala
scala> val favorites = List("risotto", "pizza")
val favorites: List[String] = List(risotto, pizza)

scala> val employeesAndFavorites = employees.zip(favorites)
val employeesAndFavorites: List[(Employee, String)] = List((Employee(Adam Johnson,34,adam.johnson@gmail.com,5000), "risotto"), (Employee(Kanye West,43,yeezy@yahoo.com,6800), "pizza"))
```

With those two lists joined together, we could then do something like print out each employee's name and their favorite food. 

```scala
scala> employees
   .zip(favorites)
   .map(tup =>
      println(s"${tup._1.name.firstName} likes ${tup._2} so much!")
   )
Adam likes risotto so much!
Kanye likes pizza so much!
val res0: List[Unit] = List((), ())
```

This has us encountering one of the first and biggest downsides to using tuples over case classes though. Our expression is not particularly easy to understand because `tup._1` and `tup._2` are not descriptive. If you don't remember that `tup._1` is an employee, and `tup._2` is the food, the string `s"${tup._1.name.firstName} likes ${tup._2} so much!"` doesn't mean a lot to you. Even if you remember those things, it takes a bit to get back to understanding what this string should show us, because we've lost a lot of context.

## Parameter untupling in functions

**WARNING! THIS SECTION IS VERY ADVANCED. IF YOU DON'T UNDERSTAND IT, DON'T FRET! UNDERSTANDING THIS SECTION IS NOT NECESSARY TO CONTINUE**

In the earliest units, we talked about expressions and statements, and simple vs complex expressions. What we didn't talk about is a very specific class of simple expressions: literals. Literals are representations of data injected directly into our code. `4` is an `Int` literal, it's the number 4, as an `Int`, injected directly into our code. If you wrote `val x = 4`, `x` would not be a literal. It is a name for `Int` data, and a simple expression, but not a literal. 

When you are defining functions, this same concept applies: `(a: Int) => a + 5` is a function literal while `fn` with the type `Int => Int` is not a function literal, just a simple expression with the type `Int => Int`.

This is important to know in order to use parameter untupling. When you tell Scala that you need a function, and that function's parameter is a tuple, then you can provide a function literal with the number of parameters that the tuple has, and Scala will interpret that as the right type.

```scala
scala> val w: (Int, Int) => Int = (a, b) => a 
val w: (Int, Int) => Int = ...
scala> //w is a function that takes two ints as parameters
scala> val x: ((Int, Int)) => Int = tup => tup._1 
val x: ((Int, Int)) => Int = ...
scala> //x is a function that takes a tuple as a parameter

scala> val y: ((Int, Int)) => Int = (a, b) => a 
val y: ((Int, Int)) => Int = ...
scala> //y is a function that takes a tuple as a parameter, but we define it with a function literal that should have the type (Int, Int) => Int

scala> val z: ((Int, Int)) => Int = w
-- [E007] Type Mismatch Error: -------------------------------------------------
1 |val z: ((Int,Int)) => Int = w
  |                            ^
  |                            Found:    (w : (Int, Int) => Int)
  |                            Required: ((Int, Int)) => Int
```

As you can see, `w` is defined with the function literal `(a, b) => a`. This is interpreted as an expression with the type `(Int, Int) => Int` based on the type assigned to `w`. `y` is defined with the exact same literal, but it's interpreted as an expression of type `((Int, Int)) => Int`, a function that takes a tuple `(Int, Int)` and returns an `Int`. You'll see that when we try to define `z`, which we've declared as having the type `((Int, Int)) => Int` with `w`, we get a type error. That's because `(Int, Int) => Int` is not the same type as `((Int, Int)) => Int`, and `w` is a simple expression of the former, not the latter. Meanwhile, the function literal `(a, b) => a` can be interpreted as either of these types, depending on the context.

Using this, it's possible to give tuples an additional bit of context. In our original expression that printed employees with their favorite food, we passed to `map` a function literal that took a tuple, but we can instead pass it a function literal that takes two parameters, and have those parameters well named...


```scala
scala> employees
   .zip(favorites)
   .map((employee, favoriteFood) =>
      println(s"${employee.name.firstName} likes ${favoriteFood} so much!")
   )
Adam likes risotto so much!
Kanye likes pizza so much!
val res0: List[Unit] = List((), ())
```

## unzip

We've seen in a previous example that `List` has a handy method named `zip` that when given a second list, will create a `List` of tuples, with the contents of the two lists joined in order. 

`unzip` is a method that does the reverse: given a list of two element tuples, it will produce a two element tuple with two lists, each containing one half of the original tuples. Let's look at this:

```scala
scala> val tupleList = List((1,2),(3,4))
val tupleList: List[(Int, Int)] = List((1,2),(3,4))

scala> tupleList.unzip
val res0: (List[Int], List[Int]) = (List(1,3), List(2,4))

scala> val odds = tupleList.unzip._1
val odds: List[Int] = List(1,3)

scala> val evens = tupleList.unzip._2
val evens: List[Int] = List(2,4)
```

Note that `unzip` will only work on Lists with tuples that are pairs. That is, it only works if the tuple has the shape `(A, B)`. Lists with tuples with three or more parameters do not work with `unzip`.

## Copy

Just like with case classes, the `copy` method exists for all tuples. It works mostly the same way too. You invoke it on a tuple, and provide the data that you want to change. The big difference in using `copy` with a tuple is the parameter names. Since you don't name tuple parameters yourself, they take on their positions as their names: `_1` for the first parameter, `_2` for the second, and so on. You can use these names with `copy` to change only one specific element of a tuple...

```scala
scala> (1,2).copy(_2 = 3)
val res0: (Int, Int) = (1,3)
```

## Practice

1. Construct a tuple with the values `"hi"` and `1.4`.
2. Can tuples be returned from methods? How about used as parameters?
3. Define a case class called `Tuples` that has two tuples as its parameters
4. What is the major disadvantage of a tuple compared to a case class?
5. **HARD** 
   ```scala
   val x: (Int, Int) => Int = (a, b) => a + b
   val nums = List.range(0,4).zip(List.range(4,8)).map(x)
   ```
   Will the above code compile? Why or why not?



# Answers

Please make sure you've attempted to answer the practice questions yourself before looking at this section.

## Case classes
1. Define a case class that stores information about a house. It should have parameters for the street address, size of the land it's on, the size of the house itself, and its cost.
   ```scala
   case class House(address: String, landSize: Double, houseSize: Double, cost: Double)
   ```
2. Construct an `Employee` named "John Doe" who is 32 years old, has the email "john.doe@aol.com" and has an income of 5 dollars per month.
   ```scala
   Employee(Name("John", "Doe"), 32, "john.doe", 5)
   ```
3. Is a case class definition a statement or an expression? Can you define a case class inside a code block? Can you define one inside a method? (try if you're not sure)
   It's a statement. You can define a case class inside a codeblock, and consequently you can define one while defining a value or a method.
4. Define a method that takes an `Employee` and outputs young if they're 20 or younger, middle aged if they're 21-50, and old if they're older than 50.
   ```scala
   def ageBracket(employee: Employee): String = 
      if employee.age <= 20 then 
         "young"
      else if employee.age > 20 && employee.age < 51 then
         "middle aged"
      else
         "old"
   ```
5. **HARD** Define a case class inside the definition of a value, and make the value be an instance of that case class. What is the result of this? Why do you think the result is what it is?
   ```scala
   val a = 
     case class A(b: Int)
     A(4)
   
   val a: Object = A(4)
   ```
   `a` here is a value of type `Object`. That's because of scoping rules. The definition of the case class `A` is not visible outside the code block it was defined in, so the type of `a` cannot be `A` because that type is unknown outside of the definition of `a`.

## Tuples
1. Construct a tuple with the values `"hi"` and `1.4`.
   ```scala
   ("hi",1.4)
   ```
2. Can tuples be returned from methods? How about used as parameters?
   Yes to both. 
3. Define a case class called `Tuples` that has two tuples as its parameters
   ```scala
   case class Tuples(a: (Int, Float), b: (String, Bool))
   ```
4. What is the major disadvantage of a tuple compared to a case class?
   A tuple doesn't preserve or give context aside from the fact that the information within the tuple is related to each other somehow. A case class gives a name to the relation of the information, and a name to each piece of information
5. **HARD** 
   ```scala
   val x: (Int, Int) => Int = (a, b) => a + b
   val nums = List.range(0,4).zip(List.range(4,8)).map(x)
   ```
   Will the above code compile? Why or why not?
   It won't compile because `x` is a simple expression with an incompatible type. It takes two parameters, and `map` needs a function that takes a tuple. `val nums = List.range(0,4).zip(List.range(4,8)).map((a,b) => a + b)` works instead because it passes a function literal to `map`, which can be interpreted as a function that takes a tuple of size 2.




