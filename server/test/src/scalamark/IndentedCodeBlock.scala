package scalamark

import scalamark.Backends.textBackend
import scalamark.md
import scalatags.Text.tags.{pre, code, ul, li, p, ol, h1, h2, hr}
import scalatags.Text.implicits.*

// ex 107-118
class IndentedCodeBlock extends munit.FunSuite:
  test("ex.107") {
    assertEquals(
      md"""|    a simple
           |      indented code block""",
      List(pre(code("""|a simple
                       |  indented code block""".stripMargin)))
    )
  }

  // needs lists
  test("ex.108".fail) {
    assertEquals(
      md"""|  - foo
           |
           |    bar""",
      List(ul(li(p("foo"), p("bar"))))
    )
  }

  // needs ordered lists
  test("ex.109".fail) {
    assertEquals(
      md"""|1.  foo
           |
           |    - bar""",
      List(ol(li(p("foo"), ul(li("bar")))))
    )
  }

  test("ex.110") {
    assertEquals(
      md"""|    <a/>
           |    *hi*
           |
           |    - one""",
      List(pre(code("""|<a/>
                       |*hi*
                       |
                       |- one""".stripMargin)))
    )
  }

  test("ex.111") {
    assertEquals(
      md"""|    chunk1
         |
         |    chunk2
         |  
         | 
         | 
         |    chunk3""",
      List(pre(code("""|chunk1
                     |
                     |chunk2
                     |
                     |
                     |
                     |chunk3""".stripMargin)))
    )
  }

  test("ex.112") {
    assertEquals(
      md"""|    chunk1
           |      
           |      chunk2""",
      List(pre(code("""|chunk1
                       |  
                       |  chunk2""".stripMargin)))
    )
  }

  test("ex.113") {
    assertEquals(
      md"""|Foo
           |    bar""",
      List(p("Foo\nbar"))
    )
  }

  test("ex.114") {
    assertEquals(
      md"""|    foo
           |bar""",
      List(pre(code("foo\n")), p("bar"))
    )
  }

  // needs headings and horizontal rules
  test("ex.115".fail) {
    assertEquals(
      md"""|# Heading
           |    foo
           |-------
           |    foo
           |----""",
      List(h1("Heading"), pre(code("foo\n")), h2("Heading"), pre(code("foo\n")))
    )
  }

  test("ex.116") {
    assertEquals(
      md"""|        foo
           |    bar""",
      List(pre(code("    foo\nbar")))
    )
  }

  test("ex.117") {
    assertEquals(
      md"""|
           |    
           |    foo
           |    """,
      List(pre(code("foo\n")))
    )
  }

  test("ex.118") {
    assertEquals(
      md"""|    foo  """,
      List(pre(code("foo  ")))
    )
  }
