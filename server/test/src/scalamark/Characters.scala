package scalamark

import scalatags.Text.tags.{p, a, pre, code, em, ul, li}
import scalatags.Text.implicits.*
import scalatags.Text.attrs.{href, title, cls}
import scalamark.md
import scalamark.Backends.textBackend

// ex 25 - 41
class Characters extends munit.FunSuite:
  test("ex.25") {
    assertEquals(
      md"""|&nbsp; &amp; &copy; &AElig; &Dcaron;
           |&frac34; &HilbertSpace; &DifferentialD;
           |&ClockwiseContourIntegral; &ngE;""",
      List(p("\u00A0 & © Æ Ď\n¾ ℋ ⅆ\n∲ ≧̸"))
    )
  }

  test("ex.26") {
    assertEquals(
      md"&#35; &#1234; &#992; &#0;",
      List(p("# Ӓ Ϡ \uFFFD"))
    )
  }

  test("ex.27") {
    assertEquals(
      md"&#X22; &#XD06; &#xcab;",
      List(p("\" ആ ಫ"))
    )
  }

  test("ex.28") {
    assertEquals(
      md"""|&nbsp &x; &#; &#x;
           |&#87654321; 
           |&#abcdef0; 
           |&ThisIsNotDefined; &hi?;""",
      List(
        p(
          "&nbsp &x; &#; &#x;\n&#87654321; \n&#abcdef0; \n&ThisIsNotDefined; &hi?;"
        )
      )
    )
  }

  test("ex.29") {
    assertEquals(
      md"&copy",
      List(p("&copy"))
    )
  }

  test("ex.30") {
    assertEquals(
      md"&MadeUpEntity;",
      List(p("&MadeUpEntity;"))
    )
  }

  test("ex.31") {
    assertEquals(
      md"<a href=\"&ouml;&ouml;.html\">",
      List(p("""<a href="öö.html">"""))
    )
  }

  // needs links to pass
  test("ex.32".fail) {
    assertEquals(
      md"""[foo](/f&ouml;&ouml; "f&ouml;f&ouml;")""",
      List(a(href := "/föö", title := "föö")("foo"))
    )
  }

  // needs link references to pass
  test("ex.33".fail) {
    assertEquals(
      md"""|[foo]
           |
           |[foo]: /f&ouml;&ouml;""",
      List(p(a(href := "/föö", title := "föö")("foo")))
    )
  }

  // needs codeblocks to pass
  test("ex.34".fail) {
    assertEquals(
      md"""|``` f&ouml;&ouml;
           |foo
           |```""",
      List(pre(code(cls := "language-föö")("foo")))
    )
  }

  test("ex.35") {
    assertEquals(
      md"`f&ouml;&ouml;`",
      List(p(code("f&ouml;&ouml;")))
    )
  }

  test("ex.36") {
    assertEquals(
      md"    f&ouml;f&ouml;",
      List(pre(code("f&ouml;f&ouml;")))
    )
  }

  // needs emphasis inlines to pass
  test("ex.37".fail) {
    assertEquals(
      md"""|&#42;foo&#42;
           |*foo*""",
      List(p("*foo*", em("foo")))
    )
  }

  // needs unordered lists to pass
  test("ex.38".fail) {
    assertEquals(
      md"""|&#42; foo
           |
           |* foo""",
      List(p("* foo"), ul(li("foo")))
    )
  }

  test("ex.39") {
    assertEquals(
      md"foo&#10;&#10;bar",
      List(p("foo\n\nbar"))
    )
  }

  test("ex.40") {
    assertEquals(
      md"&#9;foo",
      List(p("\tfoo"))
    )
  }

  test("ex.41") {
    assertEquals(
      md"[a](url &quot;tit&quot;)",
      List(p("[a](url \"tit\")"))
    )
  }
