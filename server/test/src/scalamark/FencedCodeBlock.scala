package scalamark

import scalamark.md
import scalamark.Backends.textBackend
import scalatags.Text.all.*

class FencedCodeBlock extends munit.FunSuite:
  test("ex.119") {
    assertEquals(
      md"""|```
           |<
           | >
           |```""",
      List(pre(code("<\n >\n")))
    )
  }

  test("ex.120") {
    assertEquals(
      md"""|~~~
           |<
           | >
           |~~~""",
      List(pre(code("<\n >\n")))
    )
  }

  test("ex.121") {
    assertEquals(
      md"""|``
           |foo
           |``""",
      List(p(code("foo")))
    )
  }

  test("ex.122") {
    assertEquals(
      md"""|```
           |aaa
           |~~~
           |```""",
      List(pre(code("aaa\n~~~\n")))
    )
  }

  test("ex.123") {
    assertEquals(
      md"""|~~~
           |aaa
           |```
           |~~~""",
      List(pre(code("aaa\n```\n")))
    )
  }

  test("ex.124") {
    assertEquals(
      md"""|````
           |aaa
           |```
           |``````""",
      List(pre(code("aaa\n```\n")))
    )
  }

  test("ex.125") {
    assertEquals(
      md"""|~~~~
           |aaa
           |~~~
           |~~~~""",
      List(pre(code("aaa\n~~~\n")))
    )
  }

  test("ex.126") {
    assertEquals(
      md"```",
      List(pre(code("\n")))
    )
  }

  test("ex.127") {
    assertEquals(
      md"""|`````
           |
           |```
           |aaa""",
      List(pre(code("\n```\naaa\n")))
    )
  }

  // needs quotes
  test("ex.128".fail) {
    assertEquals(
      md"""|> ```
           |> aaa
           |
           |bbb""",
      List(blockquote(pre(code("aaa\n"))), p("bbb"))
    )
  }

  test("ex.129") {
    assertEquals(
      md"""|```
           |
           |  
           |```""",
      List(pre(code("\n  \n")))
    )
  }

  test("ex.130") {
    assertEquals(
      md"""|```
           |```""",
      List(pre(code("\n")))
    )
  }

  test("ex.131") {
    assertEquals(
      md"""| ```
           | aaa
           |aaa
           |```""",
      List(pre(code("aaa\naaa\n")))
    )
  }

  test("ex.132") {
    assertEquals(
      md"""|  ```
           |aaa
           |  aaa
           |aaa
           |  ```""",
      List(pre(code("aaa\naaa\naaa\n")))
    )
  }

  test("ex.133") {
    assertEquals(
      md"""|   ```
           |   aaa
           |    aaa
           |  aaa
           |   ```""",
      List(pre(code("aaa\n aaa\naaa\n")))
    )
  }

  test("ex.134") {
    assertEquals(
      md"""|    ```
           |    aaa
           |    ```""",
      List(pre(code("```\naaa\n```")))
    )
  }

  test("ex.135") {
    assertEquals(
      md"""|```
           |aaa
           |  ```""",
      List(pre(code("aaa\n")))
    )
  }

  test("ex.136") {
    assertEquals(
      md"""|   ```
           |aaa
           |  ```""",
      List(pre(code("aaa\n")))
    )
  }

  test("ex.137") {
    assertEquals(
      md"""|```
           |aaa
           |    ```""",
      List(pre(code("aaa\n    ```\n")))
    )
  }

  test("ex.138") {
    assertEquals(
      md"""|``` ```
           |aaa""",
      List(p(code(" "), "\naaa"))
    )
  }

  test("ex.139") {
    assertEquals(
      md"""|~~~~~~
           |aaa
           |~~~ ~~""",
      List(pre(code("aaa\n~~~ ~~\n")))
    )
  }

  test("ex.140") {
    assertEquals(
      md"""|foo
           |```
           |bar
           |```
           |baz""",
      List(p("foo"), pre(code("bar\n")), p("baz"))
    )
  }

  //needs headers
  test("ex.141".fail) {
    assertEquals(
      md"""|foo
           |---
           |~~~
           |bar
           |~~~
           |# baz""",
      List(h2("foo"), pre(code("bar\n")), h1("baz"))
    )
  }

  test("ex.142") {
    assertEquals(
      md"""|```ruby
           |def foo(x)
           |  return 3
           |end
           |```""".map(_.render),
      List(pre(code(cls:= "language-ruby")("def foo(x)\n  return 3\nend\n")).render)
    )
  }

  test("ex.143") {
    assertEquals(
      md"""|~~~~    ruby startline=3 $$%@#$$
           |def foo(x)
           |  return 3
           |end
           |~~~~~~~""".map(_.render),
      List(pre(code(cls:="language-ruby")("def foo(x)\n  return 3\nend\n")).render)
    )
  }

  test("ex.144") {
    assertEquals(
      md"""|````;
           |````""".map(_.render),
      List(pre(code(cls := "language-;")("\n")).render)
    )
  }

  test("ex.145") {
    assertEquals(
      md"""|``` aa ```
           |foo""".map(_.render),
      List(p(code("aa"), "\nfoo").render)
    )
  }
  
  test("ex.146") {
    assertEquals(
      md"""|~~~ aa ``` ~~~
           |foo
           |~~~""".map(_.render),
      List(pre(code(cls := "language-aa")("foo\n")).render)
    )
  }

  test("ex.147") {
    assertEquals(
      md"""|```
           |``` aaa
           |```""",
      List(pre(code("``` aaa\n")))
    )
  }