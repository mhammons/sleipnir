package scalamark

import scalamark.md
import scalamark.Backends.textBackend
import scalatags.Text.all.*

class AtxHeadingSpec extends munit.FunSuite:
  test("ex.62") {
    assertEquals(
      md"""|# foo
           |## foo
           |### foo
           |#### foo
           |##### foo
           |###### foo""",
      List(h1("foo"), h2("foo"), h3("foo"), h4("foo"), h5("foo"), h6("foo"))
    )
  }

  test("ex.63") {
    assertEquals(
      md"####### foo",
      List(p("####### foo"))
    )
  }

  test("ex.64") {
    assertEquals(
      md"""|#5 bolt
           |
           |#hashtag""",
      List(p("#5 bolt"), p("#hashtag"))
    )
  }

  test("ex.65") {
    assertEquals(
      md"""\## foo""",
      List(p("## foo"))
    )
  }

  // needs emphasis
  test("ex.66".fail) {
    assertEquals(
      md"""# foo *bar* \*baz\*""",
      List(h1("foo ", em("bar"), " *baz*"))
    )
  }

  test("ex.67") {
    assertEquals(
      md"#                  foo                     ",
      List(h1("foo"))
    )
  }

  test("ex.68") {
    assertEquals(
      md"""| ### foo
           |  ## foo
           |   # foo""",
      List(h3("foo"), h2("foo"), h1("foo"))
    )
  }

  test("ex.69") {
    assertEquals(md"    # foo", List(pre(code("# foo"))))
  }

  test("ex.70") {
    assertEquals(
      md"""|foo
           |    # bar""",
      List(p("foo\n# bar"))
    )
  }

  test("ex.71") {
    assertEquals(
      md"""|## foo ##
           |  ###   bar    ##""",
      List(h2("foo"), h3("bar"))
    )
  }

  test("ex.72") {
    assertEquals(
      md"""|# foo ##################################
           |##### foo ##""",
      List(h1("foo"), h5("foo"))
    )
  }

  test("ex.73") {
    assertEquals(
      md"### foo ###     ",
      List(h3("foo"))
    )
  }

  test("ex.74") {
    assertEquals(
      md"### foo ### b",
      List(h3("foo ### b"))
    )
  }

  test("ex.75") {
    assertEquals(
      md"# foo#",
      List(h1("foo#"))
    )
  }

  test("ex.76") {
    assertEquals(
      md"""|### foo \###
           |## foo #\##
           |# foo \#""",
      List(h3("foo ###"), h2("foo ###"), h1("foo #"))
    )
  }

  test("ex.77") {
    assertEquals(
      md"""|****
           |## foo
           |****""",
      List(hr(), h2("foo"), hr())
    )
  }

  test("ex.78") {
    assertEquals(
      md"""|Foo bar
           |# baz
           |Bar foo""",
      List(p("Foo bar"), h1("baz"), p("Bar foo"))
    )
  }

  test("ex.79") {
    assertEquals(
      md"""|## 
           |#
           |### ###""",
      List(h2(""), h1(""), h3(""))
    )
  }