package scalamark

import scalatags.Text.tags.{hr, p, pre, code, ul, li, h2}
import scalatags.Text.implicits.*
import scalamark.Backends.textBackend
import cats.data.NonEmptyList

//ex 43 - 61
class ThematicBreakSpec extends munit.FunSuite:
  test("basic thematic break") {
    assertEquals(
      thematicBreak.parse("***"),
      Right(("", Block.ThematicBreak))
    )
  }

  test("multiple breaks") {
    assertEquals(
      thematicBreak.rep.parse("""|***
                                 |---""".stripMargin),
      Right(("", NonEmptyList(Block.ThematicBreak, List(Block.ThematicBreak))))
    )
  }
  test("ex.43") {
    assertEquals(
      md"""|***
           |---
           |___""",
      List(hr(), hr(), hr())
    )
  }

  test("ex.44") {
    assertEquals(
      md"+++",
      List(p("+++"))
    )
  }

  test("ex.45") {
    assertEquals(
      md"===",
      List(p("==="))
    )
  }

  test("ex.46") {
    assertEquals(
      md"""|--
           |**
           |__""",
      List(p("--\n**\n__"))
    )
  }

  test("ex.47") {
    assertEquals(
      md"""| ***
           |  ***
           |   ***""",
      List(hr(), hr(), hr())
    )
  }

  test("ex.48") {
    assertEquals(
      md"    ***",
      List(pre(code("***")))
    )
  }

  test("ex.49") {
    assertEquals(
      md"""|Foo
           |    ***""",
      List(p("Foo\n***"))
    )
  }

  test("ex.50") {
    assertEquals(
      md"_____________________________________",
      List(hr())
    )
  }

  test("ex.51") {
    assertEquals(
      md" - - -",
      List(hr())
    )
  }

  test("ex.52") {
    assertEquals(
      md" **  * ** * ** * **",
      List(hr())
    )
  }

  test("ex.53") {
    assertEquals(
      md"-     -      -      -",
      List(hr())
    )
  }

  test("ex.54") {
    assertEquals(
      md"- - - -    ",
      List(hr())
    )
  }

  test("ex.55") {
    assertEquals(
      md"""|_ _ _ _ a
           |
           |a------
           |
           |---a---""",
      List(p("_ _ _ _ a"), p("a------"), p("---a---"))
    )
  }

  test("ex.56") {
    assertEquals(md" *-*", List(p("*-*")))
  }

  // needs unordered lists
  test("ex.57".fail) {
    assertEquals(
      md"""|- foo
           |***
           |- bar""",
      List(ul(li("foo")), hr(), ul(li("bar")))
    )
  }

  test("ex.58") {
    assertEquals(
      md"""|Foo
           |***
           |bar""",
      List(p("Foo"), hr(), p("bar"))
    )
  }

  // needs setext headings
  test("ex.59".fail) {
    assertEquals(
      md"""|Foo
           |---
           |bar""",
      List(h2("Foo"), p("bar"))
    )
  }

  // needs unordered list
  test("ex.60".fail) {
    assertEquals(
      md"""|* Foo
           |* * *
           |* Bar""",
      List(ul(li("Foo")), hr(), ul(li("Bar")))
    )
  }

  // needs unordered list
  test("ex.61".fail) {
    assertEquals(
      md"""|- Foo
           |- * * *""",
      List(ul(li("Foo"), li(hr())))
    )
  }
