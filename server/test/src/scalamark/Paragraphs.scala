package scalamark

import scalamark.md
import scalatags.Text.tags.*
import scalatags.Text.implicits.*
import scalatags.generic.Bundle
import scalamark.Backends.textBackend

class Paragraphs extends munit.FunSuite:
  test("ex.219") {
    assertEquals(
      md"""|aaa
           |
           |bbb""",
      List(p("aaa"), p("bbb"))
    )
  }

  test("ex.220") {
    assertEquals(
      md"""|aaa
               |bbb
               |
               |ccc
               |ddd""",
      List(p("aaa\nbbb"), p("ccc\nddd"))
    )
  }

  test("ex.221") {
    assertEquals(
      md"""|aaa
               |
               |
               |bbb""",
      List(p("aaa"), p("bbb"))
    )
  }

  test("ex.222") {
    assertEquals(
      md"""|  aaa
           | bbb""",
      List(p("aaa\nbbb"))
    )
  }

  test("ex.223") {
    assertEquals(
      md"""|aaa
           |           bbb
           |                                       ccc""",
      List(p("aaa\nbbb\nccc"))
    )
  }

  test("ex.224") {
    assertEquals(
      md"""|   aaa
           |bbb""",
      List(p("aaa\nbbb"))
    )
  }

  test("ex.225") {
    assertEquals(
      md"""|    aaa
           |bbb""",
      List(pre(code("aaa\n")), p("bbb"))
    )
  }

  test("ex.226") {
    assertEquals(
      md"""|aaa     
           |bbb     """,
      List(p("aaa", br(), "bbb"))
    )
  }
