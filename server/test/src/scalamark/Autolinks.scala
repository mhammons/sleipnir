package scalamark

import scalatags.Text.tags.*
import scalatags.Text.attrs.href
import scalatags.Text.implicits.*
import scalamark.md
import scalamark.Backends.textBackend

//ex 593 - 611
class Autolinks extends munit.FunSuite:
  test("ex.593") {
    assertEquals(
      md"<http://foo.bar.baz>",
      List(p(a(href := "http://foo.bar.baz")("http://foo.bar.baz")))
    )
  }

  test("ex.594") {
    assertEquals(
      md"<http://foo.bar.baz/test?q=hello&id=22&boolean>",
      List(
        p(
          a(href := "http://foo.bar.baz/test?q=hello&id=22&boolean")(
            "http://foo.bar.baz/test?q=hello&id=22&boolean"
          )
        )
      )
    )
  }

  test("ex.595") {
    assertEquals(
      md"<irc://foo.bar:2233/baz>",
      List(p(a(href := "irc://foo.bar:2233/baz")("irc://foo.bar:2233/baz")))
    )
  }

  test("ex.596") {
    assertEquals(
      md"<MAILTO:FOO@BAR.BAZ>",
      List(p(a(href := "MAILTO:FOO@BAR.BAZ")("MAILTO:FOO@BAR.BAZ")))
    )
  }

  test("ex.597") {
    assertEquals(
      md"<a+b+c:d>",
      List(p(a(href := "a+b+c:d")("a+b+c:d")))
    )
  }

  test("ex.598") {
    assertEquals(
      md"<made-up-scheme://foo,bar>",
      List(p(a(href := "made-up-scheme://foo,bar")("made-up-scheme://foo,bar")))
    )
  }

  test("ex.599") {
    assertEquals(
      md"<http://../>",
      List(p(a(href := "http://../")("http://../")))
    )
  }

  test("ex.600") {
    assertEquals(
      md"<localhost:5001/foo>",
      List(p(a(href := "localhost:5001/foo")("localhost:5001/foo")))
    )
  }

  test("ex.601") {
    assertEquals(
      md"<http://foo.bar/baz bim>",
      List(p("<http://foo.bar/baz bim>"))
    )
  }

  test("ex.602") {
    assertEquals(
      md"""<http://example.com\/[\>""",
      List(
        p(a(href := """http://example.com\/[\""")("""http://example.com\/[\"""))
      )
    )
  }

  test("ex.603") {
    assertEquals(
      md"<foo@bar.example.com>",
      List(
        p(a(href := "mailto:foo@bar.example.com")("foo@bar.example.com"))
      )
    )
  }

  test("ex.604") {
    assertEquals(
      md"<foo+special@Bar.baz-bar0.com>",
      List(
        p(
          a(href := "mailto:foo+special@Bar.baz-bar0.com")(
            "foo+special@Bar.baz-bar0.com"
          )
        )
      )
    )
  }

  test("ex.605") {
    assertEquals(
      md"""<foo\+@bar.example.com>""",
      List(p("<foo+@bar.example.com>"))
    )
  }

  test("ex.606") {
    assertEquals(
      md"<>",
      List(p("<>"))
    )
  }

  test("ex.607") {
    assertEquals(
      md"< http://foo.bar >",
      List(p("< http://foo.bar >"))
    )
  }

  test("ex.608") {
    assertEquals(
      md"<m:abc>",
      List(p("<m:abc>"))
    )
  }

  test("ex.609") {
    assertEquals(
      md"<foo.bar.baz>",
      List(p("<foo.bar.baz>"))
    )
  }

  test("ex.610") {
    assertEquals(
      md"http://example.com",
      List(p("http://example.com"))
    )
  }

  test("ex.611") {
    assertEquals(
      md"foo@bar.example.com",
      List(p("foo@bar.example.com"))
    )
  }
