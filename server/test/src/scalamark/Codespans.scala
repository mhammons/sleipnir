package scalamark

import scalamark.md
import scalamark.Backends.textBackend
import scalatags.Text.tags.*
import scalatags.Text.implicits.*
import scalatags.Text.attrs.href

// ex 328 - 348
class Codespans extends munit.FunSuite:
  test("ex.328") {
    assertEquals(
      md"""`foo`""",
      List(p(code("foo")))
    )
  }

  test("ex.329") {
    assertEquals(
      md"``foo ` bar``",
      List(p(code("foo ` bar")))
    )
  }

  test("ex.330") {
    assertEquals(
      md"` `` `",
      List(p(code("``")))
    )
  }

  test("ex.331") {
    assertEquals(
      md"`  ``  `",
      List(p(code(" `` ")))
    )
  }

  test("ex.332") {
    assertEquals(
      md"` a`",
      List(p(code(" a")))
    )
  }

  test("ex.333") {
    assertEquals(
      md"` b `",
      List(p(code(" b ")))
    )
  }

  test("ex.334") {
    assertEquals(
      md"""|` `
              |`  `""",
      List(p(code(" "), "\n", code("  ")))
    )
  }

  test("ex.335") {
    assertEquals(
      md"""|``
              |foo
              |bar  
              |baz
              |``""",
      List(p(code("foo bar   baz")))
    )
  }

  test("ex.336") {
    assertEquals(
      md"""|``
              |foo 
              |``""",
      List(p(code("foo ")))
    )
  }

  test("ex.337") {
    assertEquals(
      md"""|`foo   bar 
                        |baz`""",
      List(p(code("foo   bar  baz")))
    )
  }

  test("ex.338") {
    assertEquals(
      md"""|`foo\`bar`""",
      List(p(code("foo\\"), "bar`"))
    )
  }

  test("ex.339") {
    assertEquals(
      md"``foo`bar``",
      List(p(code("foo`bar")))
    )
  }

  test("ex.340") {
    assertEquals(
      md"` foo `` bar `",
      List(p(code("foo `` bar")))
    )
  }

  test("ex.341") {
    assertEquals(
      md"*foo`*`",
      List(p("*foo", code("*")))
    )
  }

  test("ex.342") {
    assertEquals(
      md"""[not a `link](/foo`)""",
      List(p("[not a ", code("link](/foo"), ")"))
    )
  }

  test("ex.343") {
    assertEquals(
      md"""`<a href="`">`""",
      List((p(code("<a href=\""), "\">`")))
    )
  }

  test("ex.344".fail) {
    assertEquals(
      md"""<a href="`">`""",
      List(p(raw("""<a href="`">"""), "`"))
    )
  }

  test("ex.345") {
    assertEquals(
      md"""`<http://foo.bar.`baz>`""",
      List(p(code("<http://foo.bar."), "baz>`"))
    )
  }

  test("ex.346") {
    assertEquals(
      md"<http://foo.bar.`baz>`",
      List(p(a(href := "http://foo.bar.`baz")("http://foo.bar.`baz"), "`"))
    )
  }

  test("ex.347") {
    assertEquals(
      md"```foo``",
      List(p("```foo``"))
    )
  }

  test("ex.348") {
    assertEquals(md"`foo", List(p("`foo")))
  }
