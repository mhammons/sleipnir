package scalamark

class Html5EmailSpec extends munit.FunSuite:
  test("my email") {
    assertEquals(
      email.parseAll("markehammons@gmail.com"),
      Right((("markehammons", "gmail"), List("com")))
    )
  }

  test("weird email") {
    assertEquals(
      email.parseAll("hunbun+loveu@com"),
      Right((("hunbun+loveu", "com"), Nil))
    )
  }

  test("blankline") {
    assertEquals(
      additionalChunks.parse("""|    
                               |   a""".stripMargin),
      Right(("    \n   a", Nil))
    )
  }
