package sleipnir

import scalamark.InlineElements.*
import scalatags.text.Frag
import scalatags.Text.tags.*
import scalatags.Text.implicits.*
import scala.quoted.*
import scala.util.chaining.*

import cats.parse.Parser
import scalatags.Text.TypedTag
import cats.parse.Parser0
import cats.parse.Rfc5234.{lf, cr, alpha, digit, wsp, vchar, htab, sp, ctl}
import scalamark.minimize
import cats.data.NonEmptyList
import scalamark.autolink
import scalamark.escaped
import scalamark.entityReference
import scalamark.decimalNumericCharacterReference
import scalamark.hexadecimalNumericCharacterReference
import scalamark.InlineElements

extension (inline sc: StringContext)
  inline def md(args: Any*): List[Modifier] = ${
    mdImpl('args, 'sc)
  }

private def mdImpl(argsExpr: Expr[Seq[Any]], sc: Expr[StringContext])(using
    Quotes,
    Type[Modifier]
): Expr[List[Modifier]] =
  import quotes.reflect.report
  Parser
    .oneOf(paragraph :: Nil)
    .rep
    .parse(sc.valueOrAbort.parts.mkString.stripMargin)
    .fold(
      s => report.errorAndAbort(s.toString),
      (_, l) => Expr.ofList(l.toList.toSeq)
    )

val plaintext: QuotedParser[StringFrag] =
  (alpha | digit | wsp).rep.string.map(s => '{ StringFrag(${ Expr(s) }) })
val anytext: QuotedParser[StringFrag] =
  (vchar | wsp).rep.string.map(s => '{ StringFrag(${ Expr(s) }) })

def anyExcept(string: String) =
  (Parser.peek(Parser.not(Parser.string(string))).with1 *> Parser.anyChar.rep(
    1,
    string.size
  )).rep.string

type QuotedParser[A] = (Quotes, Type[Modifier]) ?=> Parser[Expr[A]]

val span: QuotedParser[Modifier] = Parser.recursive[Expr[Modifier]](recurse =>

  def strongP: Parser[Expr[Modifier]] = Parser
    .oneOf(List("**", "__").map(Parser.string).map { bound =>
      val boundRepetition =
        Parser
          .oneOf(
            Parser
              .defer(strongP) :: bound.map(_ => '{ StringFrag("**") }) :: Nil
          )
          .rep0
      val allExceptBound =
        Parser.until(bound).map(s => '{ StringFrag(${ Expr(s) }) })
      bound *> boundRepetition ~ Parser
        .oneOf(recurse.backtrack :: plaintext :: allExceptBound :: Nil)
        .rep <* bound
    })
    .map((b, e) => '{ strong(${ Expr.ofList((b ++ e.toList).toSeq) }*) })

  Parser.oneOf(List(strongP.withContext("strongp")))
)

val star = Parser.char('*').as('*')
val underscore = Parser.char('_').as('_')

val lineEnding = (lf | cr | cr ~ lf).as(())

val line = (Parser.until(lineEnding | Parser.end) <* (lineEnding | Parser.end))
  .withContext("line")

val blankLine =
  ((htab | sp).rep0.void.with1 *> lineEnding)
    .repUntil(Parser.end)
    .void
    .withContext("blank line")

val text: QuotedParser[StringFrag] =
  Parser.anyChar.rep.string.map(s => '{ StringFrag(${ Expr(s) }) })

val paragraph: QuotedParser[TypedTag[String]] =
  import quotes.reflect.*
  (line
    .repUntil(blankLine | Parser.end) <* (blankLine | Parser.end))
    .map(s => s.map(_.stripLeading))
    .map(s => s.init :+ s.last.stripTrailing)
    .map(s =>
      s.toList
        .mkString("\n")
        .pipe(inlineMd.parse)
        .map(_._2)
        .fold(
          e => NonEmptyList(TextualContent(s"error: ${e.toString}"), Nil),
          identity
        )
        .map(_.toExpr)
        .pipe(es => '{ p(${ Expr.ofList(es.toList) }*) })
    )

val hardLineBreak = sp.rep(2).as(HardLineBreak).soft <* lineEnding

val textualContent = Parser.anyChar.map(c => TextualContent(c.toString))

val backtick = Parser.char('`').as('`')

val backtickStringInf = backtick.rep <* Parser.not(backtick)

def backtickStringBorder(size: Int) = Parser.not(
  backtick
) *> Parser.anyChar *> backtick.rep(size, size) <* Parser.not(backtick)

val codeSpan = backtickStringInf
  .flatMap(nel =>
    nel
      .tap(_.size.tap(println))
      .pipe(_ =>
        (Parser
          .until0(
            backtickStringBorder(nel.size)
          )
          .withContext("stringy") ~ Parser.anyChar
          .withContext("anychar grabber")).string <* backtick
          .rep(nel.size, nel.size)
          .withContext("backtick border")
      )
  )
  .map(Codespan.apply)

val inlineMd: Parser[NonEmptyList[InlineElements]] = ???
  // Parser
  //   .oneOf(
  //     (hexadecimalNumericCharacterReference.backtrack | decimalNumericCharacterReference.backtrack | entityReference.backtrack) ::
  //       hardLineBreak.backtrack :: escaped :: autolink.backtrack :: codeSpan.backtrack :: backtickStringInf.string
  //         .map(
  //           TextualContent.apply
  //         ) :: textualContent :: Nil
  //   )
  //   .rep
  //   .map(_.minimize)
