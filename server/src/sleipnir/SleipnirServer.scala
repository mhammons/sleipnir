package sleipnir

import cats.effect.kernel.Async
import sttp.tapir.*
import org.http4s.blaze.server.*
import org.http4s.*
import com.comcast.ip4s.*
import sttp.tapir.server.http4s.Http4sServerInterpreter
import org.http4s.server.staticcontent.*
import cats.implicits.*
import pureconfig.ConfigSource
import scala.concurrent.ExecutionContext.global
import templates.mainTemp
import units.howToRunScalaCode


object SleipnirServer:
  val healthEndpoint = endpoint.in("_health")
  val units = endpoint.in("u0").out(htmlBodyUtf8)
  def healthLogic[F[_]: Async] =
    healthEndpoint.serverLogicPure[F](_ => Right(()))
  def unitsLogic[F[_]: Async] = units.serverLogicPure[F](_ => Right(mainTemp("How2Run",howToRunScalaCode).render))
  def stream[F[_]: Async] =
    BlazeServerBuilder[F](global).bindHttp(8080, "localhost")
      .withHttpApp(
        (Http4sServerInterpreter[F]()
          .toRoutes(List(healthLogic[F], unitsLogic[F])) <+> resourceServiceBuilder[F]("/units")
          .withPathPrefix("/units")
          .toRoutes).orNotFound
      )
      .resource