package sleipnir.units

import scalatags.Text.all._
import sleipnir.components.codeBlock
import sleipnir.md

val howToRunScalaCode = div(
  h1("How to Run Scala Code"),
  p(
    "For now, you'll be running Scala via the ",
    a(href := "https://scala-cli.virtuslab.org/install")("Scala CLI tool"),
    " developed by VirtusLab. Later lessons in Foundations and the Scala path will show you how to run Scala in the browser and on the JVM using ",
    code("mill"),
    ". Outside of these lessons, for now you should always default to running your Scala code via the Scala CLI unless otherwise specified. Otherwise you may run into unexpected errors."
  ),
  p(
    "Installing the VirtusLab Scala CLI tool is relatively easy. All you need to do is run the following two commands in the CLI:"
  ),
  codeBlock(
    "bash",
    """|curl -sSLf https://virtuslab.github.io/scala-cli-packages/scala-setup.sh | sh
        |source ~/.profile""".stripMargin
  ),
  p(
    "Once you've completed this step, you're now able to run Scala code in three different ways:"
  ),
  ul(
    li("The Scala console"),
    li("Compiling and running a Scala file"),
    li("Compiling to Javascript with ScalaJS")
  ),
  h2("Scala console"),
  p(
    "To enter the scala console you can run ",
    code("scala-cli console"),
    ". You can run hello world by entering the following:"
  ),
  codeBlock("scala", """|scala> "Hello, World!"""".stripMargin),
  p(
    "You should see the output of this command in the line below when you press enter."
  ),
  p(
    "In order to exit the Scala console, write ",
    code(":q"),
    " and press enter."
  ),
  p(
    "The Scala console is the quickest and easiest way to run and test Scala code. You will find yourself using it to test your code even when you're an experienced programmer!"
  ),
  p(
    "Unless otherwise noted, please try to run the examples in these article in the Scala console. Experimenting with Scala will help you better understand the language!"
  ),
  h2("Compiling and running a Scala file"),
  p("This approach is much closer to what you'll be doing when you're writing actual programs in Scala. Your programs will be composed of one or many ", code(".scala"), " files with an entrypoint that tells the ", a(href:="https://en.wikipedia.org/wiki/Compiler")("compiler"), " where your program should start from."),
  p("The most simple Scala program to run via this method is \"Hello, World!\", a program that outputs the text ", code("Hello, World!"), " upon execution. To create and run this program, follow these steps in VSCode:"),
  ul(
     li("Create a file named ", code("HelloWorld.scala"), " in a folder of your choosing"),
     li("Write the line ", code("""@main def program = println("Hello, World!")"""), " inside the file"),
     li("Save the file"),
     li("In the menubar, navigate to ", code("View > Terminal"), " to access the internal VSCode terminal"),
     li("Type ", code("scala-cli run HelloWorld.scala"), " into the terminal and press enter")
  ),
  """|* Create a file name `HelloWorld.scala` in a folder of your choosing
     |* Write the line `@main def program = println("Hello, World!")` inside the file
     |""".stripMargin,
  p("Your terminal should show some work done, followed by the line of text ", code("Hello, World!"),". Congratulations, you've just run your first Scala program!"),
  p("One difference between the Scala console and a proper Scala program to keep in mind is that the Scala console will give you immediate feedback about every line you enter into it, while a proper Scala program will only output text if you use ", code("println"), "."),
  md"""********hello**** winnie"""
)
