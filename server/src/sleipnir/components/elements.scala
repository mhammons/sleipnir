package sleipnir.components

import scalatags.Text.all._


def codeBlock(lang: String, codeText: String) = 
   pre(
      code(cls:=s"lang-$lang")(codeText)
   )