package sleipnir

import cats.effect.IOApp
import cats.effect.IO
import cats.effect.ExitCode

object Main extends IOApp:
   def run(args: List[String]) = 
      SleipnirServer.stream[IO].use(_ => IO.never).as(ExitCode.Success)