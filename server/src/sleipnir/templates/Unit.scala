package sleipnir.templates

import scalatags.Text.tags.*
import scalatags.Text.tags2.*
import scalatags.Text.implicits.*
import scalatags.Text.attrs.{rel, href, src}
import scalatags.Text.Frag
import scalatags.Text.{TypedTag, Modifier}

def mainTemp(titleText: String, tags: Frag*) = html(
  head(
    title(titleText),
    link(
      rel := "stylesheet",
      href := "//cdnjs.cloudflare.com/ajax/libs/highlight.js/11.5.1/styles/default.min.css"
    ),
    script(
      src := "//cdnjs.cloudflare.com/ajax/libs/highlight.js/11.5.1/highlight.min.js"
    ),
    script(
      src := "//cdnjs.cloudflare.com/ajax/libs/highlight.js/11.5.1/languages/scala.min.js"
    ),
    script("hljs.highlightAll();")
  ),
  body(tags)
)
