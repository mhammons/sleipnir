package scalamark

import cats.parse.{Parser as P}

val block = P
  .oneOf(
    thematicBreak
      .map(Some.apply)
      .backtrack :: fencedCodeBlock.map(Some.apply).backtrack :: atxHeading
      .map(
        Some.apply
      ).backtrack ::
      indentedCodeBlock.map(Some.apply) :: blankLine.as(
        None
      ) :: paragraph.map(Some.apply) :: Nil
  )
  .rep
  .map(_.toList.flatten)
