package scalamark

import scala.quoted.*
import scalatags.Text.Modifier
import scalatags.Text.StringFrag
import scalatags.Text.tags.*
import scalatags.Text.attrs.href
import scalatags.Text.implicits.*
import scalatags.Text.AttrValue
import cats.data.NonEmptyList
import scala.util.chaining.*

type Quoted[A] = Quotes ?=> Type[Modifier] ?=> Expr[A]

enum InlineElements(exprRepr: Quoted[Modifier]):
  case TextualContent(string: String)
      extends InlineElements('{ StringFrag(${ Expr(string) }) })
  case HardLineBreak extends InlineElements('{ br() })
  case Codespan(string: String) extends InlineElements({
    import quotes.reflect.report
    string.replace('\n', ' ').pipe{ 
      case str @ "  " => str
      case s" $str " => str
      case str => str
    }.pipe(s => '{code(StringFrag(${Expr(s)}))}).tap(e => report.info(e.show))
  })

  case UriAutolink(ref: String) extends InlineElements('{
    a(href := ${Expr(ref)})(StringFrag(${Expr(ref)}))
  })

  case EmailAutolink(ref: String) extends InlineElements('{
    a(href := ${Expr(s"mailto:$ref")})(StringFrag(${Expr(ref)}))
  })

  def toExpr: Quoted[Modifier] = exprRepr

extension (v: Vector[InlineElements])
  def minimize: Vector[InlineElements] =
    import InlineElements.*
    v.foldLeft(Vector.empty)((v, i) =>
      (v.last, i) match
        case (TextualContent(a), TextualContent(b)) =>
          v.init :+ TextualContent(a ++ b)
        case _ => v :+ i
    )

extension (n: NonEmptyList[InlineElements])
  def minimize: NonEmptyList[InlineElements] =
    import InlineElements.*
    println(n)
    n.tail
      .foldLeft(NonEmptyList(n.head, Nil))((r, i) =>
        (r.head, i) match
          case (TextualContent(a), TextualContent(b)) =>
            NonEmptyList(TextualContent(a ++ b), r.tail).tap(println)
          case _ => i :: r
      )
      .reverse
