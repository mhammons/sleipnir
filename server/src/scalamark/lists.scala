package scalamark

import cats.parse.{Parser as P}
import cats.parse.Rfc5234.{digit, sp, htab}

case class BulletListMarker(mark: Char, width: Int)

val bulletListMarker = ((sp | htab).rep0.with1 ~ P.charIn(
  '-' :: '+' :: '*' :: Nil
) ~ (sp | htab).rep).map { case ((left, char), right) =>
  BulletListMarker(char, left.size + right.size + 1)
}

case class OrderedListMarker(num: Int, width: Int)

val orderedListMarker = ((sp | htab).rep0.with1 ~ (digit.rep(1, 9).string <* (P
  .char('.') | P.char(')'))) ~ (sp | htab).rep).map {
  case ((left, num), right) =>
    OrderedListMarker(num.toInt, left.size + num.size + 1 + right.size)
}
