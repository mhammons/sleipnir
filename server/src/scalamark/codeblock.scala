package scalamark

import cats.parse.{Parser as P}
import cats.parse.Rfc5234.{sp, htab}
import cats.data.NonEmptyList

private val indentation = P.string("    ").withContext("spacecheck") | P
  .string("\t")
  .withContext("tabcheck")
private val indentedLine =
  ((indentation.soft *> P
    .until(
      lineEnding | P.end
    )) ~ lineEnding.rep0.string).map(_ ++ _).withContext("indented line")

private val indentedChunk =
  indentedLine.rep
    .map(_.toList.mkString)
    .withContext("chunk")

private val additionalChunks = (blankLine
  .map(s => (s.drop(4) :+ '\n').mkString)
  .rep
  .map(_.toList.mkString)
  .soft ~ indentedChunk).map(_ ++ _).rep0

val indentedCodeBlock =
  (indentedChunk ~ additionalChunks)
    .map((head, tail) => (head :: tail).mkString)
    .map(Block.IndentedCodeBlock.apply)

private val codeFence = ((P.char('`').rep(3).map(_.size -> '`') | P
  .char('~')
  .rep(3)
  .map(_.size -> '~')))

private def endFence(length: Int, char: Char) =
  lineEnding *> sp.rep0(0,3) *> P.char(char).rep(length)

private def infoStrP(fenceChar: Char) = 
  if fenceChar == '`' then 
    (P.not(P.char('`')).with1 *> P.anyChar).repUntil0(lineEnding).map(_.mkString)
  else 
    P.until0(lineEnding)

private def codeBlockLine(indentation: Int) = lineEnding *> sp.rep0(0, indentation) *> P.until0(lineEnding)
val fencedCodeBlock = (sp.rep0(0, 3).with1.soft ~ codeFence).map((l, i) => l *: i)
  .flatMap((l, i, c) =>
    infoStrP(c) ~ codeBlockLine(l.size).repUntil0(
      (endFence(i, c) <* (lineEnding | P.end)) | P.end
    ).map(_.mkString("\n") ++ "\n") <* endFence(i,c).? <* (lineEnding | P.end)
  )
  .map((infoStr, content) => Block.FencedCodeBlock(infoStr, content))
