package scalamark

import cats.parse.Rfc5234.{alpha, digit}
import cats.parse.{Parser => P}

val atext = alpha | digit | P.charIn(
  '!' :: '#' :: '$' :: '%' :: '&' :: '\'' :: '*' :: '+' :: '-' :: '/' :: '=' :: '?' :: '^' :: '_' :: '`' :: '{' :: '}' :: '|' :: '~' :: Nil
)

val letdig = alpha | digit 

val letdighyp = letdig | P.char('-').as('-')

val ldhstr = letdighyp.rep.string

val label = (letdig ~ ldhstr.rep0 ~ letdig.?).string

val email = ((atext | P.char('.').as('.')).rep.string <* P.char('@')) ~ label ~ (P.char('.') *> label).rep0
