package scalamark

import cats.parse.{Parser as P}
import cats.parse.Rfc5234.{sp, htab}
import Inline.{TextualContent, HardLinebreak}

import scala.util.chaining.*

val inlineChar = P.oneOf(
  (lineEnding.as('\n') ~ (sp | htab).rep).void
    .as('\n')
    .withContext("le")
    .backtrack :: P.anyChar :: Nil
)

val textualContent =
  inlineChar.map(_.toString).map(TextualContent.apply).withContext("textual")

val hardLinebreak =
  sp.rep(2).as(HardLinebreak) <* lineEnding <* P.not(blankLine)

val inlineElement = P
  .oneOf(
    hexadecimalNumericCharacterReference.backtrack :: decimalNumericCharacterReference.backtrack :: entityReference.backtrack ::
      hardLinebreak.backtrack :: escaped :: autolink.backtrack :: codeSpan.backtrack :: textualBacktickString :: textualContent :: Nil
  )
  .withContext("inline element")
