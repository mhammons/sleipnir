package scalamark

import cats.parse.{Parser as P}
import cats.parse.Rfc5234.wsp
import Inline.{Codespan, TextualContent}
import scala.util.chaining.*

val backtick = P.char('`').as('`')

val backtickStringInf = (backtick.rep <* P.not(backtick))

val textualBacktickString = backtickStringInf.string.map(TextualContent.apply)

def backtickStringBorder(size: Int) = P.not(
  backtick
) *> P.anyChar *> backtick.rep(size, size) <* P.not(backtick)

val codeSpan = backtickStringInf
  .flatMap(nel =>
    nel
      .pipe(_ =>
        (inlineChar
          .repUntil0(backtickStringBorder(nel.size))
          .withContext("stringy") ~ inlineChar
          .withContext("inline grabber")).string <* backtick
          .rep(nel.size, nel.size)
          .withContext("backtick border")
      )
  )
  .map(Codespan.apply)
