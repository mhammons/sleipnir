package scalamark

import cats.parse.{Parser as P}
import cats.parse.Rfc5234.{digit, hexdig}
import scala.util.chaining.*
import scala.io.Source
import io.circe.Codec
import io.circe.parser.decode
import Inline.TextualContent
import scala.util.Try

val insecure = P.char('\u0000').as('\uFFFD')

val escaped = List(
  "!",
  "\"",
  "#",
  "$",
  "%",
  "&",
  "'",
  "(",
  ")",
  "*",
  "+",
  ",",
  "-",
  ".",
  "/",
  ":",
  ";",
  "<",
  "=",
  ">",
  "?",
  "@",
  "[",
  "\\",
  "]",
  "^",
  "_",
  "`",
  "{",
  "|",
  "}",
  "~"
).map(c => s"\\$c" -> c)
  .toMap
  .pipe(P.fromStringMap)
  .map(TextualContent.apply)

case class EntityDefinition(characters: String) derives Codec.AsObject

val entityReference = P
  .defer(
    decode[Map[String, EntityDefinition]](
      Source
        .fromInputStream(
          this.getClass.getResourceAsStream("/htmlentities.json")
        )
        .mkString
    ).fold(_ => Map.empty, identity)
      .filterKeys(_.endsWith(";"))
      .toMap
      .pipe(P.fromStringMap)
  )
  .map(s => TextualContent(s.characters))

val decimalNumericCharacterReference =
  (P.char('&') *> P.char('#') *> digit.rep(1, 7).string <* P.char(';'))
    .mapFilter(_.toIntOption)
    .map {
      case 0 => 0xfffd
      case i => i
    }
    .map(_.toChar.toString)
    .map(TextualContent.apply)

val hexadecimalNumericCharacterReference =
  (P.char('&') *> P.char('#') *> (P.char('X') | P
    .char('x')) *> hexdig.rep(1, 6).string <* P.char(';'))
    .mapFilter(s => Try(Integer.parseInt(s, 16)).toOption)
    .map {
      case 0 => 0xfffd
      case i => i
    }
    .map(_.toChar.toString)
    .map(TextualContent.apply)
