package scalamark

import cats.parse.{Parser as P}
import cats.parse.Rfc5234.{lf, cr, htab, sp}

val lineEnding = (lf | cr | cr ~ lf).as(())

val line = P.until(lineEnding | P.end) <* (lineEnding | P.end)

val blankLine = ((htab | sp).as(' ').rep0.with1.soft <* lineEnding) |
  (htab | sp).as(' ').rep.map(_.toList).soft <* P.end
