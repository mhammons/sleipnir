package scalamark

import cats.parse.{Parser as P}
import cats.parse.Rfc5234.{alpha, digit, ctl, sp}
import Inline.{UriAutolink, EmailAutolink}

val scheme =
  (alpha ~ (alpha | digit | P.charIn('+' :: '.' :: '-' :: Nil)).rep).string

val absoluteUri = (scheme <* P.char(':')) ~ inlineChar.repUntil0(
  ctl | sp | P.char('<') | P.char('>')
).string
val uriAutolink =
  (P.char('<') *> absoluteUri <* P.char('>')).map((scheme, uri) =>
    UriAutolink(s"$scheme:$uri")
  )

val emailAutolink = (P.char('<') *> email <* P.char('>')).map {
  case ((address, host), hostpieces) =>
    EmailAutolink(s"$address@$host${hostpieces.mkString(".", ".", "")}")
}

val autolink = uriAutolink.backtrack | emailAutolink
