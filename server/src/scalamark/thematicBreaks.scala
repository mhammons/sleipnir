package scalamark

import cats.parse.{Parser as P}
import cats.parse.Rfc5234.{sp, htab}

val starBreak = (P.char('*') <* (sp | htab).rep0).rep(3).void
val underscoreBreak = (P.char('_') <* (sp | htab).rep0).rep(3).void
val dashBreak = (P.char('-') <* (sp | htab).rep0).rep(3).void
val thematicBreak = (((htab | sp).rep0(0, 3)).with1 *> P.oneOf(
  starBreak :: underscoreBreak :: dashBreak :: Nil
) <* (lineEnding | P.end)).as(Block.ThematicBreak)
