package scalamark

import scalatags.generic.Bundle
import scalatags.generic.Modifier
import scala.quoted.*
import scalatags.generic.TypedTag

extension [Builder, FragT, Output <: FragT](
    inline sc: StringContext
)(using b: Bundle[Builder, Output, FragT])
  inline def md(args: Any*): List[TypedTag[Builder, Output, FragT]] = ${
    mdImpl('args, 'sc, 'b)
  }
  
private def mdImpl[Builder, FragT, Output <: FragT](
    argsExpr: Expr[Seq[Any]],
    sc: Expr[StringContext],
    bundle: Expr[Bundle[Builder, Output, FragT]]
)(using
    Quotes,
    Type[Builder],
    Type[FragT],
    Type[Output]
): Expr[List[TypedTag[Builder, Output, FragT]]] =
  import quotes.reflect.report
  block
    .parse(sc.valueOrAbort.parts.mkString.stripMargin)
    .fold(
      s => report.errorAndAbort(s.toString),
      (_, l) => {
        report.info(l.toString)
        Expr.ofList(l.map(_.toExpr(bundle)).toSeq)
      }
    )
