package scalamark

import scala.quoted.*
import scala.util.chaining.*
import scalatags.generic.Bundle
import scalatags.generic.Frag
import cats.data.NonEmptyList

enum Inline:
  case TextualContent(string: String)
  case HardLinebreak
  case Codespan(string: String)
  case UriAutolink(ref: String)
  case EmailAutolink(ref: String)
  case NilInline

  def toExpr[Builder, FragT, Output <: FragT](
      bundle: Expr[Bundle[Builder, Output, FragT]]
  )(using
      Type[Builder],
      Type[FragT],
      Type[Output],
      Quotes
  ): Expr[Frag[Builder, FragT]] = this match
    case NilInline => 
      import quotes.reflect.report

      report.errorAndAbort("Un-minimized Inline group detected!!")
    case TextualContent(str) =>
      '{
        val b = $bundle
        import b.implicits.stringFrag

        stringFrag(${ Expr(str) })
      }

    case HardLinebreak =>
      '{
        val b = $bundle
        import b.tags.br

        br()
      }

    case Codespan(str) =>
      str
        .replace('\n', ' ')
        .match {
          case str @ "  " => str
          case s" $str "  => str
          case str        => str
        }
        .pipe(Expr.apply)
        .pipe(s =>
          '{
            val b = $bundle
            import b.tags.code
            import b.implicits.*

            code(stringFrag($s))
          }
        )

    case UriAutolink(ref) =>
      '{
        val b = $bundle
        import b.tags.a
        import b.attrs.href
        import b.implicits.*

        a(href := ${ Expr(ref) })(stringFrag(${ Expr(ref) }))
      }

    case EmailAutolink(ref) =>
      '{
        val b = $bundle
        import b.tags.a
        import b.attrs.href
        import b.implicits.*

        a(href := ${ (Expr(s"mailto:$ref")) })(stringFrag(${ Expr(ref) }))
      }

extension (n: NonEmptyList[Inline])
  def minim: NonEmptyList[Inline] =
    import Inline.*
    n.tail
      .foldLeft(NonEmptyList(n.head, Nil))((r, i) =>
        (r.head, i) match
          case (TextualContent(a), TextualContent(b)) =>
            NonEmptyList(TextualContent(a ++ b), r.tail)
          case _ => i :: r
      )
      .reverse


extension (l: List[Inline]) 
  def minim: List[Inline] = 
    import Inline.*
    l.foldLeft(List.empty[Inline])((r, i) =>
      (r.headOption, i) match
        case (Some(TextualContent(a)), TextualContent(b)) => 
          TextualContent(a++b) :: r.tail
        case _ => i :: r
    ).reverse