package scalamark

import cats.parse.{Parser as P}
import cats.parse.Rfc5234.{sp, htab}
import cats.data.NonEmptyList

val paragraph = {
  // demands that the line we parse is not blank, followed by shearing off the leading spaces and/or tabs
  val preamble = (sp | htab).rep0(0, 3).void.withContext("preamble")

  val endcap =
    ((sp | htab).rep0 ~ (lineEnding | P.end)).void.withContext("endcap")

  preamble.with1 *> inlineElement
    .withContext("inline element")
    .repUntil(endcap ~ (blankLine | thematicBreak | fencedCodeBlock.backtrack | atxHeading.backtrack | P.end))
    .map(_.minim)
    .map(Block.Paragraph.apply) <* endcap
}.withContext("paragraph")
