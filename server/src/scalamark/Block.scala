package scalamark

import cats.data.NonEmptyList
import scala.quoted.*
import scalatags.generic.Bundle
import scalatags.generic.Modifier
import scalatags.generic.TypedTag

enum Block:
  case Paragraph(list: NonEmptyList[Inline])
  case ListItem(bs: NonEmptyList[Block])
  case IndentedCodeBlock(content: String)
  case FencedCodeBlock(infoString: String, content: String)
  case ThematicBreak
  case Heading(level: Int, elements: List[Inline])

  def toExpr[Builder, FragT, Output <: FragT](
      bundle: Expr[Bundle[Builder, Output, FragT]]
  )(using
      Type[Builder],
      Type[FragT],
      Type[Output],
      Quotes
  ): Expr[TypedTag[Builder, Output, FragT]] = this match
    case Paragraph(l) =>
      val contents = Expr.ofList(l.map(_.toExpr(bundle)).toList.toSeq)
      '{
        val b = $bundle

        import b.tags.p

        p($contents*)
      }

    case ListItem(bs) => ???

    case IndentedCodeBlock(content) =>
      '{
        val b = $bundle
        import b.tags.{pre, code}
        import b.implicits.stringFrag
        pre(code(stringFrag(${ Expr(content) })))
      }

    case ThematicBreak =>
      '{
        val b = $bundle
        import b.tags.hr
        hr()
      }

    case FencedCodeBlock(info, content) =>
      '{
        val b = $bundle
        import b.tags.{pre, code}
        import b.attrs.cls
        import b.implicits.*

        pre(
          ${
            info.stripLeading.split(" ").headOption match
              case Some(lang) if info.stripLeading.nonEmpty =>
                '{
                  val l = ${ Expr(lang) }
                  code(cls := s"language-$l")(stringFrag(${ Expr(content) }))
                }
              case _ =>
                '{
                  code(stringFrag(${ Expr(content) }))
                }
          }
        )
      }

    case Heading(level, inlineElements) =>
      val contents =
        Expr.ofList(inlineElements.map(_.toExpr(bundle)).toList.toSeq)
      '{
        val b = $bundle
        import b.tags.{h1, h2, h3, h4, h5, h6}
        import b.implicits.*

        val tag = ${
          level match
            case 1 =>
              '{ h1($contents*) }
            case 2 =>
              '{ h2($contents*) }
            case 3 =>
              '{ h3($contents*) }
            case 4 =>
              '{ h4($contents*) }
            case 5 =>
              '{ h5($contents*) }
            case 6 =>
              '{ h6($contents*) }
        }

        tag
      }
