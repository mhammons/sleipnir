package scalamark

import cats.parse.{Parser as P}
import cats.parse.Rfc5234.{sp, htab}
import cats.data.NonEmptyList
import scala.collection.immutable.ArraySeq

val closing = (sp.rep0.void ~ (lineEnding | P.end)).void
val closingHash = (sp.rep.soft ~ P.char('#').rep ~ closing).void
val heading = (P.not(closingHash | closing) *> sp.rep *> inlineElement.repUntil(closingHash | closing)).map(_.minim.toList)
val intro = sp.rep0(0, 3).with1.soft *> P.char('#').rep(1,6).map(_.size)
val atxHeading = (intro ~ heading.?.map(
  _.fold(List(Inline.TextualContent("")))(identity)
)
  <* (closingHash | closing)).map((l, e) =>
  Block.Heading(l, e)
)
